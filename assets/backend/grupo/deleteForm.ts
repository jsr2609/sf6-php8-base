import createNotification from "../../helpers/createNotification";
import {blockUI, unblockUI} from "../../helpers/blockUI";
import createConfirmation from "../../helpers/createConfirmation";
import $ from "jquery";

export default function initEliminar() {
  const deleteForm: JQuery<HTMLFormElement> = $(
    "form[name=grupo-eliminar]"
  );
  deleteForm.on("submit", (e: JQuery.SubmitEvent<HTMLFormElement>) => {
    e.preventDefault();
    const deleteConfirmation = createConfirmation(
      "Confirme la eliminación del registro."
    );
    deleteConfirmation.fire().then((result) => {
      if (result.isConfirmed) {
        const action = deleteForm.attr("action");
        const formData = new FormData(deleteForm[0]);
        $.ajax({
          url: action,
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
        })
          .done((data, textStatus, jqXHR) => {
            createNotification("success", data.message);
            $(location).attr("href", data.url_redirect);
          })
          .fail((jqXHR, textStatus, errorThrown) => {})
          .always(() => {});
      }
    });
  });
}
