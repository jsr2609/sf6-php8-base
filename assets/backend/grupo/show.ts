import Modal from "bootstrap/js/dist/modal";
import initEliminar from "./deleteForm";
import $ from "jquery";

export default function initConsultar() 
{
  const modal = new Modal("#modal-show-grupo");
  modal.show();

  initEliminar();
}