import $ from "jquery";
import  'datatables.net-bs5';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';
import initGuardar from "./new";
import initEditar from "./edit";
import initConsultar from "./show";
import {blockUI, unblockUI} from "../../helpers/blockUI";

$(() => {
  
  const $listaLugarAccidente = $("#lista-grupo");
  $listaLugarAccidente.DataTable();

  const $btnNuevo: JQuery<HTMLButtonElement> = $("#btn-add-grupo");
  $btnNuevo.on("click", function() {
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initGuardar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });

  

  $("#lista-grupo").on("click", ".btn-edit-grupo", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initEditar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
    
  });

  

  $("#lista-grupo").on("click", ".btn-show-grupo", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      },
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initConsultar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });  
});