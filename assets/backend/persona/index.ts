import $ from "jquery";
import  'datatables.net-bs5';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';
import initGuardar from "./new";
import initEditar from "./edit";
import initConsultar from "./show";
import {blockUI, unblockUI} from "../../helpers/blockUI";

$(() => {
  
  const $listaLugarAccidente = $("#lista-persona");
  $listaLugarAccidente.DataTable();

  const $btnNuevo: JQuery<HTMLButtonElement> = $("#btn-add-persona");
  $btnNuevo.on("click", function() {
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initGuardar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });

  

  $("#lista-persona").on("click", ".btn-edit-persona", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initEditar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
    
  });

  

  $("#lista-persona").on("click", ".btn-show-persona", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      },
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initConsultar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });  
});