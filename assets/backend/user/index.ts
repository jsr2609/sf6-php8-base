import $ from "jquery";
import "jquery-validation";
import  'datatables.net-bs5';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';
import initGuardar from "./new";
import initEditar from "./edit";
import initConsultar from "./show";
import {blockUI, unblockUI} from "../../helpers/blockUI";
import "../../helpers/configDataTable";
import "../../helpers/configjQueryValidation";


$(() => {
  
  const $listaLugarAccidente: JQuery<HTMLTableElement> = $("#lista-user");
  
  const dataUrl = $listaLugarAccidente.attr('data-url');
  
//   $('#lista-user tfoot th').each(function () {
//     var title = $(this).text();
//     $(this).html('<input type="text" placeholder="Search ' + title + '" />');
// });

  $('#lista-user thead tr').clone(true).appendTo( '#lista-user thead' );
  const numberColumns = $('#lista-user thead tr:eq(1) th').length;
  const $listaLugarAccidenteDT = $listaLugarAccidente.DataTable({
    "orderCellsTop": true,
    "processing": true,
    "serverSide": true,
    "ajax": dataUrl,
    order: [[0, 'desc']],
    "columnDefs":[
      {"targets":0,"width":"20%"},
      {"targets":1, "width":"30%"},
      {"targets":2, "width":"30%"}, 
      {"targets":3, "className":"text-end", "width":"15%", "orderable": false},                
                     
    ],
    initComplete: function () {     
    },   
  });

  $('#lista-user thead tr:eq(1) th').each(function (i) {
    if ((i + 1) < numberColumns) {
      var title = $(this).text();
      $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');

      $('input', this).on('keyup change', function <HTMLInputElement>() {
        if ($listaLugarAccidenteDT.search() !== this.value) {
          $listaLugarAccidenteDT.columns(i)
            .search(this.value)
            .draw();
        }
      });
    } else {
      $(this).html('');
    }

  });

  
  
  
  const $btnNuevo: JQuery<HTMLButtonElement> = $("#btn-add-user");
  $btnNuevo.on("click", function() {
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initGuardar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });

  

  $("#lista-user").on("click", ".btn-edit-user", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initEditar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
    
  });

  

  $("#lista-user").on("click", ".btn-show-user", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      },
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initConsultar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });  
});