const reglasValidacionForm = {
    rules: {
        //"user[password][first]": 'password_regex',
        "user[password][second]": {
            equalTo: "#user_password_first"
        }
    },
    messages: {
        "user[password][second]": {
          equalTo: "La contrasseña nueva debe ser igual en los dos campos."
        }
    }
}

export default reglasValidacionForm;