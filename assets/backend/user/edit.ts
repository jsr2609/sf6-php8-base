import Modal from "bootstrap/js/dist/modal";
import createNotification from "../../helpers/createNotification";
import createConfirmation from "../../helpers/createConfirmation";
import {blockUI, unblockUI} from "../../helpers/blockUI";
import redirect from "../../helpers/redirect";
import initEliminar from "./deleteForm";
import reglasValidacionForm from "./reglasValidacion";
import $ from "jquery";

//import form from './form';

export default function initEditar() {
  const modal = new Modal("#modal-edit-user");
  
  modal.show();

  const form: JQuery<HTMLFormElement> = $("form[name=user]");
  form.validate(reglasValidacionForm);

  form.on("submit", (e: JQuery.SubmitEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (form.valid()) {
      const action = form.attr("action");
      const formData = new FormData(form[0]);
      $.ajax({
        url: action,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: function() {
          blockUI();
        }
      })
        .done((data, textStatus, jqXHR) => {
          createNotification("success", data.message);
          redirect(data.url_redirect);
        })
        .fail((jqXHR, textStatus, errorThrown) => {})
        .always(() => {
          unblockUI();
        });
    } else {
      createNotification("error", "Se encontraron errores en el formulario");
    }
  });

  const btnActualizar = $("#btn-update-user");
  btnActualizar.on("click", (e: JQuery.ClickEvent<HTMLButtonElement>) => {
    form.trigger("submit");
  });

  initEliminar();
}
