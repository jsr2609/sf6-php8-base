import $ from "jquery";
import "block-ui";

const  blockUI = (message: string | null = "Procesando...") => {
  $.blockUI({
    message: '<div class="spinner-border text-primary" role="status" style="width: 3rem; height: 3rem;">    <span class="sr-only">spinner...</span>  </div><div><span class="text-muted">Cargando...</span></div>',
    overlayCSS: { backgroundColor: '#000', opacity: 0.8 },
    css: {
        border: 'none',
        left: '45%',
        padding: '5px',
        backgroundColor: '#fff',

        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        width: "200px",
        // color: '#fff',
        // height: '150px'
    },
    baseZ: 2065,
    blockMsgClass: 'blockMsg',
});
}

const  unblockUI = () => {
  $.unblockUI();
}

export {blockUI, unblockUI};