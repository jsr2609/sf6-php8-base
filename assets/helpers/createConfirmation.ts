import Swal, { SweetAlertIcon } from "sweetalert2";

export default function createConfirmation(
  description: string | null = "Confirme realizar la acción.",
  title: string | null = "Confirmación", 
  icon: SweetAlertIcon | null = "info"
): typeof Swal {
  const swalWithBootstrapButtons: typeof Swal = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-primary me-2",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
    icon: icon,
    title: title,
    text: description,

    showCancelButton: true,
    confirmButtonText: '<i class="fas fa-check-circle"></i> Confirmar ',
    cancelButtonText: '<i class="fas fa-times-circle"></i> Cancelar',
    
  });

  return swalWithBootstrapButtons;
}
