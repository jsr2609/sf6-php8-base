import $ from "jquery";
import  'datatables.net-bs5';
import dataTableSpanish from './dataTableSpanish';

$.extend(true, $.fn.dataTable.defaults, {
  orderCellsTop: true,
  language: dataTableSpanish,
  dom:
        "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'Bl>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
});

