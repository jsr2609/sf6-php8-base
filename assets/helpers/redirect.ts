import $ from "jquery";


export default function redirect(url: string): void
{
  $(location).attr("href", url);
}