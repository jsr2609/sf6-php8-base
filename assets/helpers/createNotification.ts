import toastr from 'toastr';

const createNotification = (type: String, message: String, title: String | null = null) => {
  toastr[type](message, title);
}

export default createNotification;