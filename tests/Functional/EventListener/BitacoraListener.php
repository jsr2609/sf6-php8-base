<?php

declare(strict_types=1);

namespace App\Tests\Functional\EventListener;

use Doctrine\ORM\Event\OnFlushEventArgs;
use App\EventListener\BitacoraListenerInterface;

class BitacoraListener implements BitacoraListenerInterface
{
    public function onFlush(OnFlushEventArgs $args): void
    {
    }
}
