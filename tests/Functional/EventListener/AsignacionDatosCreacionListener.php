<?php

declare(strict_types=1);

namespace App\Tests\Functional\EventListener;

use Doctrine\ORM\Event\PrePersistEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
use App\EventListener\AsignacionDatosCreacionListenerInterface;

class AsignacionDatosCreacionListener implements AsignacionDatosCreacionListenerInterface
{
    public function __construct(
        private Security $security
    )
    {
    }
    
    public function prePersist(PrePersistEventArgs $args): void
    {
        //En el entorno de test, los datos se llenan en los fixtures
    }

    public function estaEnEntidadesAIgnorar(string $class): bool
    {
        $entidadesAIgnorar = [
            'App\Entity\Bitacora',
        ];

        return in_array($class, $entidadesAIgnorar);
    }
}
