<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTestBase extends WebTestCase
{
    protected ?KernelBrowser $authenticatedClient = null;

    public function setUp(): void
    {
        $this->authenticatedClient = $this->createAuthenticatedClient();
    }

    protected function createAuthenticatedClient(): KernelBrowser
    {
        $client =  static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        return $client;
    }

    public function generarDataDT(int $cantidadColumnas, array $dataExtra): array
    {
        $informacionColumnas = $this->generarInformacionColumnas($cantidadColumnas);
        $data = [
            "draw" => "1",
            "columns" => $informacionColumnas,
            "order" => [
                0 =>  [
                    "column" => "0",
                    "dir" => "asc",
                ]
            ],
            "start" => "0",
            "length" => "10",
            "search" =>  [
                "value" => "",
                "regex" => "false",
            ],
        ];

        return \array_merge($data, $dataExtra);
    }

    private function generarInformacionColumnas(int $cantidadColumnas): array
    {
        $data = [];
        for ($i = 0; $i < $cantidadColumnas; $i++) {
            $data[$i] = [
                "data" => $i,
                "name" => "",
                "searchable" => "true",
                "orderable" => "true",
                "search" =>  [
                    "value" => "",
                    "regex" => "false",
                ]
            ];
        }

        return $data;
    }
}
