<?php

namespace App\Tests\Functional\Controller\Backend;

use App\Repository\UserRepository;
use App\Tests\Functional\FunctionalTestBase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends FunctionalTestBase
{
    private static string $baseURL = "/admin/user";

    public function testIndex()
    {
        $crawler = $this->authenticatedClient->request('GET', self::$baseURL . '/');

        $response = $this->authenticatedClient->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // $crawlerTable = $crawler->filter('table#lista-User');

        // self::assertCount(1, $crawlerTable);
    }

    // public function testRowsDataTable(): void
    // {
    //     // El test funciona para validar la respuesta del controlador
    //     // y la respuesta del servicio NegocioDTService
    //     $dataExtra = [];

    //     $data = $this->generarDataDT(1, $dataExtra);        

    //     $crawler = $this->authenticatedClient->xmlHttpRequest('GET', self::$baseURL . 'rows/data_table', $data);
    //     $response = $this->authenticatedClient->getResponse();
    //     self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    // }

    public function testNew(): void
    {
        $crawler = $this->authenticatedClient->xmlHttpRequest('GET', self::$baseURL . '/new');
        $response = $this->authenticatedClient->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $crawlerForm = $crawler->filter('form[name=user]');
        self::assertCount(1, $crawlerForm);

        //Inicia test Guardar Registro
        $form = $crawlerForm->form();
        $selectGrupos = $form['user']['grupos'];

        $opcionesDisponiblesGrupos = $selectGrupos->availableOptionValues();

        //Los datos para el formulario
        $values = $form->getPhpValues();
        

        $data = [
            'user' => [
                'email' => 'usuario_test_new@gmail.com',
                'nombre' => 'USUARIO',
                'apellidos' => 'PARA TEST NEW',
                'password' => [
                    'first'  => 'Prueba1$3',
                    'second' => 'Prueba1$3',
                ],
                'grupos' => [$opcionesDisponiblesGrupos[0]],
                'permisos' =>  [],
                '_token' => $values['user']['_token'],
            ],
        ];

        $this->authenticatedClient->xmlHttpRequest(
            $form->getMethod(),
            $form->getUri(),
            $data,
            $form->getPhpFiles(),
            []
        );
        //Se obtiene la respuesta de guardar el formulario
        $response = $this->authenticatedClient->getResponse();
        //Se valida el código esperado con el código de la respuesta
        self::assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testEdit(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');
        
        $crawler = $this->authenticatedClient->xmlHttpRequest('GET', self::$baseURL . '/'.$testUser->getId()->toRfc4122().'/edit');
        $response = $this->authenticatedClient->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $crawlerForm = $crawler->filter('form[name=user]');
        self::assertCount(1, $crawlerForm);

        //Inicia test Guardar Registro
        $form = $crawlerForm->form();

        //Los datos para el formulario
        $values = $form->getPhpValues();

        $data = [
            'user' => [
                'email' => 'usuario_test_edit@gmail.com',
                'nombre' => 'USUARIO',
                'apellidos' => 'PARA TEST NEW',
                'password' => [
                    'first'  => 'Prueba1$3',
                    'second' => 'Prueba1$3',
                ],
                'grupos' => $values['user']['grupos'],
                'permisos' =>  [],
                '_token' => $values['user']['_token'],
            ],
        ];

        $this->authenticatedClient->xmlHttpRequest(
            $form->getMethod(),
            $form->getUri(),
            $data,
            $form->getPhpFiles(),
            []
        );
        //Se obtiene la respuesta de guardar el formulario
        $response = $this->authenticatedClient->getResponse();
       // dd($response);
        //Se valida el código esperado con el código de la respuesta
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());


    }

    public function testDelete(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@gmail.com');
        //Se llama al controlador de editar porque regresa el formulario para eliminar
        $crawler = $this->authenticatedClient->xmlHttpRequest('GET', self::$baseURL . '/'.$testUser->getId()->toRfc4122().'/edit');
        $response = $this->authenticatedClient->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $crawlerForm = $crawler->filter('form[name=user-eliminar]');
        self::assertCount(1, $crawlerForm);

        //Inicia test eliminar Registro
        $form = $crawlerForm->form();

        //Los datos para el formulario
        $values = $form->getPhpValues();
        $data = [
            "_method" => "DELETE",
            "_token" => $values['_token'],
        ];

        $this->authenticatedClient->xmlHttpRequest(
            $form->getMethod(),
            $form->getUri(),
            $data,
            $form->getPhpFiles(),
            []
        );
        //Se obtiene la respuesta de guardar el formulario
        $response = $this->authenticatedClient->getResponse();
        //Se valida el código esperado con el código de la respuesta
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
