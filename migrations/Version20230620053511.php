<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230620053511 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bitacora (id UUID NOT NULL, entidad VARCHAR(255) NOT NULL, fecha_movimiento TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, cambio JSON DEFAULT NULL, accion VARCHAR(60) NOT NULL, observaciones VARCHAR(255) DEFAULT NULL, usuario UUID NOT NULL, ip VARCHAR(120) NOT NULL, entidad_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN bitacora.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN bitacora.usuario IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN bitacora.entidad_id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bitacora');
    }
}
