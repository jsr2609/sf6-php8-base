<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230622183751 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE permiso ADD fecha_creacion TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE permiso ADD usuario_creo UUID NOT NULL');
        $this->addSql('ALTER TABLE permiso ADD fecha_actualizacion TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE permiso ADD usuario_actualizo UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN permiso.usuario_creo IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN permiso.usuario_actualizo IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE permiso DROP fecha_creacion');
        $this->addSql('ALTER TABLE permiso DROP usuario_creo');
        $this->addSql('ALTER TABLE permiso DROP fecha_actualizacion');
        $this->addSql('ALTER TABLE permiso DROP usuario_actualizo');
    }
}
