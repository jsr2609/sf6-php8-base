<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230618193300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_grupo (user_id UUID NOT NULL, grupo_id UUID NOT NULL, PRIMARY KEY(user_id, grupo_id))');
        $this->addSql('CREATE INDEX IDX_6ECC608BA76ED395 ON user_grupo (user_id)');
        $this->addSql('CREATE INDEX IDX_6ECC608B9C833003 ON user_grupo (grupo_id)');
        $this->addSql('COMMENT ON COLUMN user_grupo.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN user_grupo.grupo_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE user_grupo ADD CONSTRAINT FK_6ECC608BA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_grupo ADD CONSTRAINT FK_6ECC608B9C833003 FOREIGN KEY (grupo_id) REFERENCES grupo (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" DROP grupos');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN roles TO permisos');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_grupo DROP CONSTRAINT FK_6ECC608BA76ED395');
        $this->addSql('ALTER TABLE user_grupo DROP CONSTRAINT FK_6ECC608B9C833003');
        $this->addSql('DROP TABLE user_grupo');
        $this->addSql('ALTER TABLE "user" ADD grupos JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN permisos TO roles');
    }
}
