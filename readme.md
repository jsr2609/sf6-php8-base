# SIRPP

## Contenedores

Para iniciar la configuración de los contenedores copiar los siguientes archivos.

* `docker-compose.dist.yml` a `docker-compose.yml`. 
* `Makefile.dist` a `Makefile`.

### docker-compose.yml

Configurar el nombre de los contenedores y sus respectivas configuraciones.

### Makefile

Configurar el nombre del contenedor que tiene PHP `DOCKER_BE`.

Revisar los comandos que se ejecutan con Docker Compose, para la versión antigua es `docker-compose`y para la nueva versión es `docker compose`.

### Iniciar los contenedores

Iniciar los contenedores con `make run`.

## Instalar Dependencias

Ingresar al contenedor con el comando `make ssh-be`.

### Composer 

Instalar los vendor con el comando `composer install`. 

### Node

Instalar los node_modules con el comando `npm install`

Una vez instalados, para producción se generan los archivos de JavaScript con el comando `npm run build`. El comando genera los archivos JavaScript dentro de la carpeta `public/build`.

Para trabajar en desarrollo es con el comando `npm run watch`, los archivos JavaScript dentro de `public/build` se iran actualizando de forma automática.

## Variables de entorno

Se tiene que crear el archivo `.env.local` para guardar las variables de entorno, en el archivo `.env` solo se podrán agregar variables comentadas, que servirán de referencia para agregarlas al `.env.local`.

## Base de datos

Crear la base de datos y ejecutar las migraciones con el siguiente comando `doctrine:migrations:migrate`. 

### Cargando fixtures

Una vez creadas las tablas carga los datos de prueba con el comando `hautelook:fixtures:load`.

Importante, en el archivo `services.yml` comentar las líneas de los siguentes servicios:

* App\EventListener\AsignacionFechasEntidadSubscriber
* App\EventListener\BitacoraListenerInterface