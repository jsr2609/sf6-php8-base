{% extends 'backend/layout/main.html.twig' %}



{% block page_title %}<i class="fa-solid fa-desktop"></i> <?= $entity_class_name ?> index{% endblock %}
{% block page_content %}
<div class="mb-2">
<button type="button" id="btn-add-<?= $entity_command_var ?>" class="btn btn-primary" data-url="{{ path('<?= $route_name ?>_new') }}"><i class="fas fa-plus-circle" aria-hidden="true"></i> Nuevo</button>
</div>
    <table id="lista-<?= $entity_command_var ?>" class="table">
        <thead>
            <tr>
<?php foreach ($entity_fields as $field): ?>
                <th><?= ucfirst($field['fieldName']) ?></th>
<?php endforeach; ?>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
        {% for <?= $entity_twig_var_singular ?> in <?= $entity_twig_var_plural ?> %}
            <tr>
<?php foreach ($entity_fields as $field): ?>
                <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
<?php endforeach; ?>
                <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" title="Editar" class="btn btn-sm btn-primary btn-edit-<?= $entity_command_var ?>" data-url="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"> <i class="fa-solid fa-edit"></i></button>
                    <button type="button" title="Consultar" class="btn btn-sm btn-info btn-show-<?= $entity_command_var ?>" data-url="{{ path('<?= $route_name ?>_show', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"><i class="fa-solid fa-search"></i></button>
</div>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <div id="contenedor-modales"></div>

    
{% endblock %}


{% block stylesheets %}
{{parent()}}
{{ encore_entry_link_tags('backend.<?= $entity_command_var ?>') }}
{% endblock %}
{% block javascripts %}
{{parent()}}
{{ encore_entry_script_tags('backend.<?= $entity_command_var ?>') }}
{% endblock %}
