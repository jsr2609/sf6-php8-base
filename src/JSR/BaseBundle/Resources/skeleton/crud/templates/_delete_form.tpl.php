<div class="mb-2">
    <form name="<?= $entity_command_var ?>-eliminar" id="form-delete-<?= $entity_command_var ?>" method="post" action="{{ path('<?= $route_name ?>_delete', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}" <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" value="{{ csrf_token('delete' ~ <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>) }}">

        <button id="btn-delete-<?= $entity_command_var ?>" class="btn btn-danger">
            <span>
                <i class="fas fa-trash"></i>
                <span>{{ button_label|default('Eliminar') }}</span>
            </span>
        </button>
    </form>
</div>