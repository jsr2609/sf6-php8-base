import Modal from "bootstrap/js/dist/modal";
import "jquery-validation";
import createNotification from '../../../helpers/createNotification';
import {blockUI, unblockUI} from "../../../helpers/blockUI";
import $ from "jquery";
import redirect from "../../../helpers/redirect"

//import form from './form';

export default function initGuardar() {
  const form: JQuery<HTMLFormElement> = $("form[name=<?= $form_name ?>]");
  form.validate();

  form.on("submit", (e) => {
    e.preventDefault();
    if (form.valid()) {
      const action = form.attr("action");
      const formData = new FormData(form[0]);
      $.ajax({
        url: action,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: () => {
          blockUI();
        }
      })
        .done((data, textStatus, jqXHR) => {
          createNotification("success", data.message);
          redirect(data.url_redirect);
        })
        .fail((jqXHR, textStatus, errorThrown) => {})
        .always(() => {
          unblockUI();
        });
    } else {
      createNotification("error", "Se encontraron errores en el formulario");
    }
  });

  const btnGuardar = $("#btn-create-<?= $entity_command_var ?>");
  btnGuardar.on("click", (e) => {
    console.log("click");
    form.trigger("submit");
  });

  const modal = new Modal("#modal-add-<?= $entity_command_var ?>");
  modal.show();
}
