import $ from "jquery";
import  'datatables.net-bs5';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';
import initGuardar from "./new";
import initEditar from "./edit";
import initConsultar from "./show";
import {blockUI, unblockUI} from "../../../helpers/blockUI";

$(() => {
  
  const $listaLugarAccidente = $("#lista-<?= $entity_command_var ?>");
  $listaLugarAccidente.DataTable();

  const $btnNuevo: JQuery<HTMLButtonElement> = $("#btn-add-<?= $entity_command_var ?>");
  $btnNuevo.on("click", function() {
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initGuardar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });

  

  $("#lista-<?= $entity_command_var ?>").on("click", ".btn-edit-<?= $entity_command_var ?>", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      }
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initEditar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
    
  });

  

  $("#lista-<?= $entity_command_var ?>").on("click", ".btn-show-<?= $entity_command_var ?>", function(e: JQuery.ClickEvent<HTMLButtonElement>) {
    e.preventDefault();
    const url = $(this).data("url");
    $.ajax({
      url: url,
      beforeSend: function() {
        blockUI();
      },
    })
    .done((data, textStatus, jqXHR) => {
      $("#contenedor-modales").html(data);
      initConsultar();
      
    })
    .fail((jqXHR, textStatus, errorThrown) => {

    })
    .always(() => {
      unblockUI();
    });
  });  
});