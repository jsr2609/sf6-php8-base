<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
use App\EventListener\AsignacionDatosActualizacionListenerInterface;

class AsignacionDatosActualizacionListener implements AsignacionDatosActualizacionListenerInterface
{
    public function __construct(
        private Security $security
    )
    {
    }
    
    public function preUpdate(PreUpdateEventArgs $args): void
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $entity = $args->getObject();

        if(!$this->estaEnEntidadesAIgnorar(get_class($entity))) {
            if(property_exists($entity, 'fechaActualizacion')) {
                $entity->setFechaActualizacion(new \DateTime());
            }

            if(property_exists($entity, 'usuarioActualizo')) {
                $entity->setUsuarioActualizo($user->getId());
            }            
        }
    }

    public function estaEnEntidadesAIgnorar(string $class): bool
    {
        $entidadesAIgnorar = [
            'App\Entity\Bitacora',
        ];

        return in_array($class, $entidadesAIgnorar);
    }
}
