<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\Events;
use App\Repository\UserRepository;
use App\Service\EncodePasswordService;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;

/**
 * Codifica el password del usuario durante la creación
 */
#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: User::class)]
class UserPasswordActualizacionListener
{

    public function __construct(
        private EncodePasswordService $encodePasswordService,
        private UserRepository $userRepository
    )
    {
        
    }
    
    public function preUpdate(User $user, PreUpdateEventArgs $args): void
    {
        /** @var User $entity */
        $entity = $args->getObject();
        //$entityManager = $args->getObjectManager();

        if($entity instanceof User) {
            $this->actualizarPassword($entity);
        }
        
        
    }

    private function actualizarPassword(User $entity): void
    {
        $password = $entity->getPassword();
        
        if(!$password){
            $passwordActual = $this->userRepository->obtenerPasswordActual($entity->getId());
            $entity->setPassword($passwordActual);
        } else {
            $entity->setPassword($this->encodePasswordService->encode($entity->getPassword()));
        }
        
    }

    
}
