<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\Event\PrePersistEventArgs;

interface AsignacionDatosCreacionListenerInterface extends AsignacionDatosEntidadListenerInterface
{
    public function prePersist(PrePersistEventArgs $args): void;
}
