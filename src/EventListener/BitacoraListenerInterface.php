<?php

namespace App\EventListener;

use Doctrine\ORM\Event\OnFlushEventArgs;

interface BitacoraListenerInterface
{
    public function onFlush(OnFlushEventArgs $args): void;
}
