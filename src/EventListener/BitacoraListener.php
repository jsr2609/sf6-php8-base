<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\UnitOfWork;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
use App\EventListener\BitacoraListenerInterface;
use App\Service\BitacoraService;
use Symfony\Component\HttpFoundation\RequestStack;

class BitacoraListener implements BitacoraListenerInterface
{
    private \DateTime $fechaMovimiento;
    private User $user;
    private string $ip;

    public function __construct(
        private RequestStack $requestStack,
        private Security $security,
        private BitacoraService $bitacoraService
    )
    {
        $this->fechaMovimiento = new \DateTime();
        $this->user = $security->getUser();
        $this->ip = $requestStack->getCurrentRequest()->getClientIp();

    }
    public function onFlush(OnFlushEventArgs $args): void
    {
        $user = $this->security->getUser();
        $direccionIP = $this->requestStack->getCurrentRequest()->getClientIp();

        $objectManager = $args->getObjectManager();
        /** @var UnitOfWork $uow */
        $uow = $objectManager->getUnitOfWork();

        //Recorrer las entidas nuevas a guardar
        foreach ($uow->getScheduledEntityInsertions() as $key => $entity) {
            if(!$this->comprobarEntidadesAIgnorar(get_class($entity))) {
                $bitacora = $this->bitacoraService->generarBitacora(
                    get_class($entity),
                    $entity->getId(),
                    [],
                    'CREACIÓN',
                    [],
                    $this->fechaMovimiento,
                    $this->user->getId(),
                    $this->ip,
                    null

                );

                $objectManager->persist($bitacora);
                $meta = $objectManager->getClassMetadata(get_class($bitacora));
                $uow->computeChangeSet($meta, $bitacora);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $key => $entity) {
            if(!$this->comprobarEntidadesAIgnorar(get_class($entity))) {
                $changesSet = $uow->getEntityChangeSet($entity);
                $bitacora = $this->bitacoraService->generarBitacora(
                    get_class($entity),
                    $entity->getId(),
                    $changesSet,
                    'ACTUALIZACIÓN',
                    [],
                    $this->fechaMovimiento,
                    $this->user->getId(),
                    $this->ip,
                    null

                );

                $objectManager->persist($bitacora);
                $meta = $objectManager->getClassMetadata(get_class($bitacora));
                $uow->computeChangeSet($meta, $bitacora);
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $key => $entity) {
            if(!$this->comprobarEntidadesAIgnorar(get_class($entity))) {
                $bitacora = $this->bitacoraService->generarBitacora(
                    get_class($entity),
                    $entity->getId(),
                    [],
                    'ELIMINACIÓN',
                    [],
                    $this->fechaMovimiento,
                    $this->user->getId(),
                    $this->ip,
                    null
                );

                $objectManager->persist($bitacora);
                $meta = $objectManager->getClassMetadata(get_class($bitacora));
                $uow->computeChangeSet($meta, $bitacora);
            }
        }

        foreach ($uow->getScheduledCollectionDeletions() as $key => $collection) {
           //Pendiente la eliminadicón de colecciones
        }

        foreach ($uow->getScheduledCollectionUpdates() as $key => $collection) {
            //Pendiente la actualización de colecciones
        }


        

    }

    private function comprobarEntidadesAIgnorar(string $class): bool
    {
        $entidadesAIgnorar = [
            'App\Entity\Bitacora',
        ];

        return in_array($class, $entidadesAIgnorar);
    }
}
