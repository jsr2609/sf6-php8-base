<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;

interface AsignacionDatosActualizacionListenerInterface extends AsignacionDatosEntidadListenerInterface
{
    public function preUpdate(PreUpdateEventArgs $args): void;
}
