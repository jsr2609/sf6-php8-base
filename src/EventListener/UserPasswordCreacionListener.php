<?php

namespace App\EventListener;

use App\Entity\User;
use App\Service\EncodePasswordService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;

/**
 * Codifica el password del usuario durante la creación
 */
#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: User::class)]
class UserPasswordCreacionListener
{

    public function __construct(
        private EncodePasswordService $encodePasswordService
    )
    {
        
    }
    
    public function prePersist(User $user, PrePersistEventArgs $args): void
    {
        /** @var User $entity */
        $entity = $args->getObject();
        //$entityManager = $args->getObjectManager();

        if($entity instanceof User) {
            $entity->setPassword($this->encodePasswordService->encode($entity->getPassword()));
        }
    }

    
}
