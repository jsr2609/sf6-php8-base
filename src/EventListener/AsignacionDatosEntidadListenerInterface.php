<?php

declare(strict_types=1);

namespace App\EventListener;

interface AsignacionDatosEntidadListenerInterface
{
    public function estaEnEntidadesAIgnorar(string $class): bool;
}
