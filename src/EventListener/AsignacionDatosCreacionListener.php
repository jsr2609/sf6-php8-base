<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\Event\PrePersistEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
use App\EventListener\AsignacionDatosCreacionListenerInterface;

class AsignacionDatosCreacionListener implements AsignacionDatosCreacionListenerInterface
{
    public function __construct(
        private Security $security
    )
    {
    }
    
    public function prePersist(PrePersistEventArgs $args): void
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $objectManager = $args->getObjectManager();
        $entity = $args->getObject();

        if(!$this->estaEnEntidadesAIgnorar(get_class($entity))) {
            if(property_exists($entity, 'fechaCreacion')) {
                $entity->setFechaCreacion(new \DateTime());
            }

            if(property_exists($entity, 'usuarioCreo')) {
                $entity->setUsuarioCreo($user->getId());
            }      
        }
    }

    public function estaEnEntidadesAIgnorar(string $class): bool
    {
        $entidadesAIgnorar = [
            'App\Entity\Bitacora',
        ];

        return in_array($class, $entidadesAIgnorar);
    }
}
