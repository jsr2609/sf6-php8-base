<?php

namespace App\Exception\Database;

use App\Exception\Database\NotNullException;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;

class PrepareDatabaseException
{
    public static function prepare(string $message, string $class): void
    {
        switch($class) {
            case NotNullConstraintViolationException::class:
                NotNullException::prepare($message);
                break;
            default:
                DatabaseException::savingEntity($message);
        }
    }
}
