<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\PermisoRepository;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;

class RecuperarArregloPermisosService
{
    public function __construct(
        private PermisoRepository $permisoRepository
    )
    {
        
    }

    public function recuperar():array
    {
        $permisosNombre = $this->permisoRepository->recuperarArregloNombres();
        $permisosNombreArreglo = [];

        foreach ($permisosNombre as $key => $permisosNombre) {
            $permisosNombreArreglo[$permisosNombre['nombre']] = $permisosNombre['nombre'];
        }

        return $permisosNombreArreglo;
    }
}
