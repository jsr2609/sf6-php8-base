<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\GrupoRepository;

class RecuperarArregloGruposService
{
    public function __construct(
        private GrupoRepository $grupoRepository
    )
    {
        
    }

    public function recuperar(): array
    {
        $grupos = $this->grupoRepository->recuperarArregloNombres();
        $gruposArreglo = [];

        foreach ($grupos as $key => $grupo) {
            $gruposArreglo[$grupo['nombre']] = $grupo['id'];
        }

        return $gruposArreglo;
    }
}
