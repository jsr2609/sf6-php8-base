<?php

namespace App\Service;

use App\Utils\BaseDT;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Routing\RouterInterface;


class UserDTService
{
    private $em;
    private $router;
    private $security;
    private $status;

    public function __construct(EntityManagerInterface $em, RouterInterface $router, Security $security)
    {
        $this->em = $em;
        $this->router = $router;
        $this->security = $security;
    }

    // Aqui se definen las columnas en el orden a mostrar
    public function getData($request, $columns, $buttons)
    {
        
        //dd($request['id']);
        if (!$columns) {
            $columns = array(
                array('db' => 'email', 'dt' => 0),
                array('db' => 'nombre', 'dt' => 1),
                array('db' => 'apellidos',  'dt' => 2),
            );
        }

        $connection = $this->em->getConnection();

        $data = $this->simple($request, $connection, 'public.user', 'id', $columns, $buttons);


        return $data;
    }

    private function simple($request, $conn, $table, $primaryKey, $columns, $buttons)
    {
        $bindings = array();
        $db = $conn;

        // Crea la cadena SQL a partir de la solicitud
        $limit = BaseDT::limit($request, $columns);
        $order = BaseDT::order($request, $columns);
        $where = BaseDT::filter($request, $columns, $bindings);

        $where = $this->addCustomFilters($where);

        // Consulta prinicipal para obtener los datos
        $data = BaseDT::sql_exec(
            $db,
            $bindings,
            "SELECT id, " . implode(", ", BaseDT::pluck($columns, 'db')) .
                " FROM $table $where $order $limit"

        );

        // Longitud del conjunto de datos después del filtrado
        $resFilterLength = BaseDT::sql_exec(
            $db,
            $bindings,
            "SELECT COUNT({$primaryKey}) FROM   $table $where"
        );
        $recordsFiltered = $resFilterLength[0]['count'];

        // Longitud total del conjunto de datos 
        $resTotalLength = BaseDT::sql_exec(
            $db,
            $bindings,
            "SELECT COUNT({$primaryKey}) FROM   $table $where"
        );
        $recordsTotal = $resTotalLength[0]['count'];

        /*
         * Output
         */
        return array(
            "draw"            => isset($request['draw']) ?
                intval($request['draw']) :
                0,
            "recordsTotal"    => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data"            => $this->data_output($columns, $data, $buttons)
        );
    }

    private function addCustomFilters($where)
    {
        /*
         * Example
        $condition = ' id < 30'; 
        $where = $this->addCondition($where, $condition);
        */
        

        if ($where === 'WHERE ') {
            $where = '';
        }

        return $where;
    }


    public function addCondition($where, $condicion)
    {
        if ($where === '') {
            $where = $where . ' WHERE ' . $condicion;
        } else {
            $where = $where . ' AND ' . $condicion;
        }

        return $where;
    }


    /**
     * Retorna un arreglo con los registros de la consulta
     */
    private function data_output($columns, $data, $buttons)
    {
        $out = array();

        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();

            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];

                // ¿Hay un formateador?
                if (isset($column['formatter'])) {
                    if (empty($column['db'])) {
                        $row[$column['dt']] = $column['formatter']($data[$i]);
                    } else {
                        $row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                    }
                } else {
                    if (!empty($column['db'])) {
                        $row[$column['dt']] = $data[$i][$columns[$j]['db']];
                    } else {
                        $row[$column['dt']] = "";
                    }
                }
            }


            if ($buttons) {
                $row[] = $buttons($data[$i]);
                $out[] = $row;
            } else {
                $urlShow = $this->router->generate('admin_app_user_show', ['id' => $data[$i]['id']]);
                $urlEdit = $this->router->generate('admin_app_user_edit', ['id' => $data[$i]['id']]);
                $btnShow = '<button class="btn-show-user btn btn-info btn-sm" title="Consultar" data-url="'.$urlShow.'"><i class="fas fa-search"></i></button>';
                $btnEdit = '<button class="btn-edit-user btn btn-primary btn-sm" title="Editar" data-url="'.$urlEdit.'"><i class="fas fa-edit"></i></button>';
                $btnGroup = '<div class="btn-group" role="group">'.$btnEdit.$btnShow.'</div>';
                $row[] =$btnGroup;
                $out[] = $row;
            }
        }

        return $out;
    }
}
