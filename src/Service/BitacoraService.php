<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Bitacora;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Uid\Uuid;

class BitacoraService
{
    public function __construct()
    {
        
    }

    public function generarBitacora(
        string $entidad,
        Uuid $entidadId,
        array $changeset,
        string $tipoAccion,
        array $formatters,
        \DateTime $fechaMovimiento,
        Uuid $userId,
        string $ip,
        ?string $observaciones = null
    ): Bitacora
    {
        $cambiosJson = $this->analizarCambios($changeset, $formatters);

        return $this->newBitacora($entidad, $entidadId, $fechaMovimiento, $cambiosJson, $tipoAccion, $ip, $userId, $observaciones);
    }

    private function analizarCambios(array $changeset, array $formatters): array
    {
        $cambios = [];

        foreach ($changeset as $k1 => $cambio) {
            $modificacion = [];
            $tieneFormato = array_key_exists($k1, $formatters);
            foreach ($cambio as $k2 => $value) {
                $keyTxt = ($k2 == 0) ? 'anterior' : 'actual';
                if($tieneFormato) {                    
                    $mod[$keyTxt] = $this->recuperarValor($value, $k1, $formatters[$k1]['formatter']);
                } else {
                    $mod[$keyTxt] = $this->recuperarValor($value);
                }
                
            }

            $cambios[$k1] = $mod;
        }

        return $cambios;
    }

    private function recuperarValor($value, $formatter = null)
    {
        if($formatter) {
            return $formatter($value);
        }
        if($value instanceof \DateTime) {
            return $value->format('Y-m-d h:i:s');
        }

        if($value instanceof \DateTimeImmutable) {
            return $value->format('Y-m-d h:i:s');
        }

        if(is_object($value)) {
            return $value->getId();
        }

        return $value;
    }

    public function newBitacora(string $entidad, Uuid $entidadId, \DateTime $fechaMovimiento, array $cambiosJson, string $tipoAccion, string $direccionIP, Uuid $userId, ?string $observaciones): Bitacora
    {
        $bitacora = new Bitacora();
        $bitacora->setEntidad($entidad);
        $bitacora->setEntidadId($entidadId);
        $bitacora->setFechaMovimiento($fechaMovimiento);
        $bitacora->setCambio($cambiosJson);
        $bitacora->setAccion($tipoAccion);
        $bitacora->setUsuario($userId);
        $bitacora->setIp($direccionIP);
        $bitacora->setObservaciones($observaciones);

        return $bitacora;
    }


}
