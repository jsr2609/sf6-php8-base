<?php

namespace App\Controller\Backend;

use App\Entity\Grupo;
use App\Exception\Database\PrepareDatabaseException;
use App\Form\GrupoType;
use App\JSR\BaseBundle\Utils\FormErrorsSerializer;
use App\Repository\GrupoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route('/admin/grupo')]
class GrupoController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'admin_app_grupo_index', methods: ['GET'])]
    public function index(GrupoRepository $grupoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('backend/grupo/index.html.twig', [
            'grupos' => $grupoRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'admin_app_grupo_new', methods: ['GET', 'POST'])]
    public function new(Request $request, GrupoRepository $grupoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $grupo = new Grupo();
        $form = $this->createForm(GrupoType::class, $grupo, [
            'action' => $this->generateUrl('admin_app_grupo_new'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->persist($grupo);
                $this->entityManager->flush();
            } catch (\Throwable $th) {
                PrepareDatabaseException::prepare('Error al guardar el registro.', \get_class($e));
            }

            $urlRedirect = $this->generateUrl('admin_app_grupo_index', [
            ]);
            $datos = [
                'code' => JsonResponse::HTTP_CREATED,
                'id' => $grupo->getId(),
                'message' => 'El registro se creo correctamente.',
                'url_redirect' => $urlRedirect,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_CREATED);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);
            $datos = [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->render('backend/grupo/new.html.twig', [
            'grupo' => $grupo,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_grupo_show', methods: ['GET'])]
    public function show(Grupo $grupo): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('backend/grupo/show.html.twig', [
            'grupo' => $grupo,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_app_grupo_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Grupo $grupo, GrupoRepository $grupoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $form = $this->createForm(GrupoType::class, $grupo, [
            'action' => $this->generateUrl('admin_app_grupo_edit', [
                'id' => $grupo->getId(),
            ]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }

            $this->addFlash('success', 'El Grupo se actualizó correctamente.');

            $urlRedirect = $this->generateUrl('admin_app_grupo_index', [
            ]);

            $datos = [
                'code' => JsonResponse::HTTP_OK,
                'id' => $grupo->getId(),
                'message' => 'El registro se actualizó correctamente.',
                'url_redirect' => $urlRedirect,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_OK);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);

            $datos = [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->render('backend/grupo/edit.html.twig', [
            'grupo' => $grupo,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_grupo_delete', methods: ['POST'])]
    public function delete(Request $request, Grupo $grupo, GrupoRepository $grupoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        if ($this->isCsrfTokenValid('delete'.$grupo->getId(), $request->request->get('_token'))) {
            try {
                $this->entityManager->remove($grupo);
                $this->entityManager->flush();

                $urlRedirect = $this->generateUrl('admin_app_grupo_index', [
                ]);

                $datos = [
                    'code' => JsonResponse::HTTP_OK,
                    'message' => 'El registró se eliminó correctamente.',
                    'url_redirect' => $urlRedirect,
                ];

                return new JsonResponse($datos, JsonResponse::HTTP_OK);
            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }
        } else {
            throw new \Exception('El token no es valido');
        }
    }
}
