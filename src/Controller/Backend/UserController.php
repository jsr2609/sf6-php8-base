<?php

namespace App\Controller\Backend;

use App\Entity\User;
use App\Form\UserType;
use App\Service\UserDTService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\JSR\BaseBundle\Util\FormErrorsSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Exception\Database\PrepareDatabaseException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route('/admin/user')]
class UserController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'admin_app_user_index', methods: ['GET'])]
    public function index(UserRepository $userRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('backend/user/index.html.twig', [
            'users' => [],
        ]);
    }

    #[Route(path: 'rows/data/table', name: 'admin_user_rows_data_table', methods: ['GET'])]
    public function rowsDataTable(Request $request, UserDTService $userDTService): Response
    {
        $buttons = null;

        $data = $userDTService->getData($request->query->all(), null, $buttons);

        $response = new JsonResponse($data);
        
  
        return $response;
    }

    #[Route('/new', name: 'admin_app_user_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('admin_app_user_new'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                throw new \Exception($e->getMessage());
            }

            $urlRedirect = $this->generateUrl('admin_app_user_index', [
            ]);
            $datos = [
                'code' => JsonResponse::HTTP_CREATED,
                'id' => $user->getId(),
                'message' => 'El registro se creo correctamente.',
                'url_redirect' => $urlRedirect,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_CREATED);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);
            $datos = [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->render('backend/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('backend/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_app_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $form = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('admin_app_user_edit', [
                'id' => $user->getId(),
            ]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                throw new \Exception($e->getMessage());
            }

            $this->addFlash('success', 'El User se actualizó correctamente.');

            $urlRedirect = $this->generateUrl('admin_app_user_index', [
            ]);

            $datos = [
                'code' => JsonResponse::HTTP_OK,
                'id' => $user->getId(),
                'message' => 'El registro se actualizó correctamente.',
                'url_redirect' => $urlRedirect,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_OK);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);

            $datos = [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->render('backend/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            try {
                $this->entityManager->remove($user);
                $this->entityManager->flush();

                $urlRedirect = $this->generateUrl('admin_app_user_index', [
                ]);

                $datos = [
                    'code' => JsonResponse::HTTP_OK,
                    'message' => 'El registró se eliminó correctamente.',
                    'url_redirect' => $urlRedirect,
                ];

                return new JsonResponse($datos, JsonResponse::HTTP_OK);
            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }
        } else {
            throw new \Exception('El token no es valido');
        }
    }
}
