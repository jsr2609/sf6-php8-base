<?php

namespace App\Controller\Backend;

use App\Entity\Persona;
use App\Form\PersonaType;
use App\Repository\PersonaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\JSR\BaseBundle\Utils\FormErrorsSerializer;
use App\Exception\Database\PrepareDatabaseException;


#[Route('/admin/persona')]




class PersonaController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'admin_app_persona_index', methods: ['GET'])]
    public function index(PersonaRepository $personaRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('backend/persona/index.html.twig', [
            'personas' => $personaRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'admin_app_persona_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PersonaRepository $personaRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }
        
        $persona = new Persona();
        $form = $this->createForm(PersonaType::class, $persona, array(
            'action' => $this->generateUrl('admin_app_persona_new'),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            try {
                $this->entityManager->persist($persona);
                $this->entityManager->flush();
            } catch (\Throwable $th) {
                PrepareDatabaseException::prepare('Error al guardar el registro para Tipo de Auto.', \get_class($e));
            }

            $urlRedirect =  $this->generateUrl('admin_app_persona_index', [
                
            ]);
            $datos = array(
                'code' => JsonResponse::HTTP_CREATED,
                'id' => $persona->getId(),
                'message' => 'El registro se creo correctamente.',
                'url_redirect' => $urlRedirect,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_CREATED);

        } elseif($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);
            $datos = array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
                
            );

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);

        } 
        
        
        return $this->render('backend/persona/new.html.twig', [
            'persona' => $persona,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_persona_show', methods: ['GET'])]
    public function show(Persona $persona): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }
        
        return $this->render('backend/persona/show.html.twig', [
            'persona' => $persona,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_app_persona_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Persona $persona, PersonaRepository $personaRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $form = $this->createForm(PersonaType::class, $persona, array(
            'action' => $this->generateUrl('admin_app_persona_edit', array(
                'id' => $persona->getId(),
            )),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }

            $this->addFlash('success', 'El Persona se actualizó correctamente.');

            $urlRedirect =  $this->generateUrl('admin_app_persona_index', [
                
            ]);

            $datos = array(
                'code' => JsonResponse::HTTP_OK,
                'id' => $persona->getId(),
                'message' => 'El registro se actualizó correctamente.',
                'url_redirect' => $urlRedirect,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_OK);

        } elseif($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);

            $datos = array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);

        } 
        
        return $this->render('backend/persona/edit.html.twig', [
            'persona' => $persona,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_persona_delete', methods: ['POST'])]
    public function delete(Request $request, Persona $persona, PersonaRepository $personaRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        if ($this->isCsrfTokenValid('delete'.$persona->getId(), $request->request->get('_token'))) {
            try {
                $this->entityManager->remove($persona);
                $this->entityManager->flush();

                $urlRedirect =  $this->generateUrl('admin_app_persona_index', [
                    
                ]);

                $datos = [
                    'code' => JsonResponse::HTTP_OK,
                    'message' => 'El registró se eliminó correctamente.',
                    'url_redirect' => $urlRedirect
                ];

                return new JsonResponse($datos, JsonResponse::HTTP_OK);

            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }


        } else {
            throw new \Exception('El token no es valido');
        }
    }
}
