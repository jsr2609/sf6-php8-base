<?php

namespace App\Controller\Backend;

use App\Entity\Permiso;
use App\Exception\Database\PrepareDatabaseException;
use App\Form\PermisoType;
use App\JSR\BaseBundle\Utils\FormErrorsSerializer;
use App\Repository\PermisoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route('/admin/permiso')]
class PermisoController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'admin_app_permiso_index', methods: ['GET'])]
    public function index(PermisoRepository $permisoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('backend/permiso/index.html.twig', [
            'permisos' => $permisoRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'admin_app_permiso_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PermisoRepository $permisoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $permiso = new Permiso();
        $form = $this->createForm(PermisoType::class, $permiso, [
            'action' => $this->generateUrl('admin_app_permiso_new'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->persist($permiso);
                $this->entityManager->flush();
            } catch (\Throwable $th) {
                PrepareDatabaseException::prepare($th->getMessage(), \get_class($th));
            }

            $urlRedirect = $this->generateUrl('admin_app_permiso_index', [
            ]);
            $datos = [
                'code' => JsonResponse::HTTP_CREATED,
                'id' => $permiso->getId(),
                'message' => 'El registro se creo correctamente.',
                'url_redirect' => $urlRedirect,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_CREATED);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);
            $datos = [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->render('backend/permiso/new.html.twig', [
            'permiso' => $permiso,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_permiso_show', methods: ['GET'])]
    public function show(Permiso $permiso): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('backend/permiso/show.html.twig', [
            'permiso' => $permiso,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_app_permiso_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Permiso $permiso, PermisoRepository $permisoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $form = $this->createForm(PermisoType::class, $permiso, [
            'action' => $this->generateUrl('admin_app_permiso_edit', [
                'id' => $permiso->getId(),
            ]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                throw $e;
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }

            $this->addFlash('success', 'El Permiso se actualizó correctamente.');

            $urlRedirect = $this->generateUrl('admin_app_permiso_index', [
            ]);

            $datos = [
                'code' => JsonResponse::HTTP_OK,
                'id' => $permiso->getId(),
                'message' => 'El registro se actualizó correctamente.',
                'url_redirect' => $urlRedirect,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_OK);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);

            $datos = [
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            ];

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->render('backend/permiso/edit.html.twig', [
            'permiso' => $permiso,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_app_permiso_delete', methods: ['POST'])]
    public function delete(Request $request, Permiso $permiso, PermisoRepository $permisoRepository): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')// Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        if ($this->isCsrfTokenValid('delete'.$permiso->getId(), $request->request->get('_token'))) {
            try {
                $this->entityManager->remove($permiso);
                $this->entityManager->flush();

                $urlRedirect = $this->generateUrl('admin_app_permiso_index', [
                ]);

                $datos = [
                    'code' => JsonResponse::HTTP_OK,
                    'message' => 'El registró se eliminó correctamente.',
                    'url_redirect' => $urlRedirect,
                ];

                return new JsonResponse($datos, JsonResponse::HTTP_OK);
            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }
        } else {
            throw new \Exception('El token no es valido');
        }
    }
}
