<?php

namespace App\Form;

use App\Entity\Grupo;
use App\Service\RecuperarArregloPermisosService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GrupoType extends AbstractType
{
    public function __construct(
        private RecuperarArregloPermisosService $recuperarArregloPermisosService
    )
    {
        
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $opcionesRoles = $this->recuperarArregloPermisosService->recuperar();
        $builder
            ->add('nombre')
            ->add('descripcion')
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices' => $opcionesRoles,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Grupo::class,
        ]);
    }
}
