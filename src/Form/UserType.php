<?php

namespace App\Form;

use App\Entity\Grupo;
use App\Entity\User;
use App\Form\EventListener\AddPasswordFieldSubscriber;
use App\Service\RecuperarArregloPermisosService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserType extends AbstractType
{
    public function __construct(
        private RecuperarArregloPermisosService $recuperarArregloPermisosService
    )
    {
        
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $opcionesPermiso = $this->recuperarArregloPermisosService->recuperar();

        $builder
            
            
            ->add('email', EmailType::class, [
                'required' => true,
            ])
            ->add('nombre')
            ->add('apellidos')
            ->addEventSubscriber(new AddPasswordFieldSubscriber())
            
            ->add('grupos', EntityType::class, [
                'multiple' => true,
                'class' => Grupo::class,
            ])
            ->add('permisos', ChoiceType::class, [
                'multiple' => true,
                'choices' => $opcionesPermiso,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
