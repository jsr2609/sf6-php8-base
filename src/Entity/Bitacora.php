<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Symfony\Component\Uid\Uuid;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BitacoraRepository;
use Symfony\Bridge\Doctrine\Types\UuidType;

#[ORM\Entity(repositoryClass: BitacoraRepository::class)]
class Bitacora
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\CustomIdGenerator(class: "doctrine.uuid_generator")]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255)]
    private ?string $entidad = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fechaMovimiento = null;

    #[ORM\Column(nullable: true)]
    private array $cambio = [];

    #[ORM\Column(length: 60)]
    private ?string $accion = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $observaciones = null;

    #[ORM\Column(type: 'uuid', nullable: false)]
    private ?Uuid $usuario = null;

    #[ORM\Column(length: 120)]
    private ?string $ip = null;

    #[ORM\Column(type: 'uuid', nullable: false)]
    private ?Uuid $entidadId = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getEntidad(): ?string
    {
        return $this->entidad;
    }

    public function setEntidad(string $entidad): static
    {
        $this->entidad = $entidad;

        return $this;
    }

    public function getFechaMovimiento(): ?\DateTimeInterface
    {
        return $this->fechaMovimiento;
    }

    public function setFechaMovimiento(\DateTimeInterface $fechaMovimiento): static
    {
        $this->fechaMovimiento = $fechaMovimiento;

        return $this;
    }

    public function getCambio(): array
    {
        return $this->cambio;
    }

    public function setCambio(?array $cambio): static
    {
        $this->cambio = $cambio;

        return $this;
    }

    public function getAccion(): ?string
    {
        return $this->accion;
    }

    public function setAccion(string $accion): static
    {
        $this->accion = $accion;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones(?string $observaciones): static
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getUsuario(): ?Uuid
    {
        return $this->usuario;
    }

    public function setUsuario(?Uuid $usuario): static
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): static
    {
        $this->ip = $ip;

        return $this;
    }

    public function getEntidadId(): ?Uuid
    {
        return $this->entidadId;
    }

    public function setEntidadId(?Uuid $entidadId): static
    {
        $this->entidadId = $entidadId;

        return $this;
    }
}
