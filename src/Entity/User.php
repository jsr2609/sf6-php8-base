<?php

namespace App\Entity;

use App\EventListener\UserEntityListener;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\EntityListeners;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]

class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    private $id;

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'string', length: 50, unique: true)]
    private $email;

    #[ORM\Column(type: 'string', length: 50)]
    private $nombre;

    #[ORM\Column(type: 'string', length: 80)]
    private $apellidos;

    #[ORM\ManyToMany(targetEntity: Grupo::class)]
    private Collection $grupos;

    #[ORM\Column(nullable: true)]
    private ?array $permisos = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable:true)]
    private ?\DateTimeInterface $fechaCreacion = null;

    #[ORM\Column(type: 'uuid', nullable: true)]
    private ?Uuid $usuarioCreo = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $fechaActualizacion = null;

    #[ORM\Column(type: 'uuid', nullable: true)]
    private ?Uuid $usuarioActualizo = null;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->grupos = new ArrayCollection();
    }   

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->permisos;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
        
        foreach ($this->grupos as $key => $grupo) {
            $roles = array_merge($roles, $grupo->getRoles());
        }

        return array_unique($roles);
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * @return Collection<int, Grupo>
     */
    public function getGrupos(): Collection
    {
        return $this->grupos;
    }

    public function addGrupo(Grupo $grupo): static
    {
        if (!$this->grupos->contains($grupo)) {
            $this->grupos->add($grupo);
        }

        return $this;
    }

    public function removeGrupo(Grupo $grupo): static
    {
        $this->grupos->removeElement($grupo);

        return $this;
    }

    public function getPermisos(): ?array
    {
        return $this->permisos;
    }

    public function setPermisos(?array $permisos): static
    {
        $this->permisos = $permisos;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion): static
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getUsuarioCreo(): ?Uuid
    {
        return $this->usuarioCreo;
    }

    public function setUsuarioCreo(?Uuid $usuarioCreo): static
    {
        $this->usuarioCreo = $usuarioCreo;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): static
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getUsuarioActualizo(): ?Uuid
    {
        return $this->usuarioActualizo;
    }

    public function setUsuarioActualizo(?Uuid $usuarioActualizo): static
    {
        $this->usuarioActualizo = $usuarioActualizo;

        return $this;
    }

}
